--[[--
File              : devicons.lua
Author            : Jeff LANCE <email@jefflance.me>
Date              : 20.07.2022
Last Modified Date: 22.07.2022
Last Modified By  : Jeff LANCE <email@jefflance.me>
--]]--

require'nvim-web-devicons'.setup {
 -- globally enable default icons (default to false)
 -- will get overriden by `get_icons` option
 default = true;
}
