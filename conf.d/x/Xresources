! ------------------------------------------------------------------------------
! Theme Configuration
! ------------------------------------------------------------------------------
!#include ".Xresources.d/themes/jellybeans.res"

! ------------------------------------------------------------------------------
! ROFI Configuration
! ------------------------------------------------------------------------------
!#include ".Xresources.d/rofi/jellybeans.res"

! ------------------------------------------------------------------------------
! Xft fonts Configuration
! ------------------------------------------------------------------------------
Xft.dpi:                96
Xft.antialias:          true
Xft.rgba:               rgb
Xft.hinting:            true
Xft.hintstyle:          hintslight
Xft.autohint:           false
Xft.lcdfilter:          lcddefault

! ------------------------------------------------------------------------------
! SSH-ASKPASS
! ------------------------------------------------------------------------------
ssh-askpass.Dialog.font:                  xft:roboto,mono-11:regular
ssh-askpass.?.foreground:                 rgb:b6/b6/b6
ssh-askpass.?.background:                 rgb:00/00/00
ssh-askpass.topShadowColor:               rgb:5f/5f/5f
ssh-askpass.bottomShadowColor:            rgb:24/24/24
ssh-askpass.borderColor:                  rgb:00/00/00
ssh-askpass.shadowThickness:              1
ssh-askpass.borderWidth:                  0
ssh-askpass.horizontalSpacing:            5
ssh-askpass.verticalSpacing:              6

ssh-askpass.Button.font:                  Roboto
ssh-askpass.Button.shadowThickness:       2
ssh-askpass.Button.borderWidth:           0
ssh-askpass.Button.horizontalSpacing:     5
ssh-askpass.Button.verticalSpacing:       2

ssh-askpass.Indicator.foreground:         rgb:f0/54/4c
ssh-askpass.Indicator.background:         rgb:55/55/55
ssh-askpass.Indicator.shadowThickness:    1
ssh-askpass.Indicator.borderWidth:        0
ssh-askpass.Indicator.height:             7
ssh-askpass.Indicator.width:              15
ssh-askpass.Indicator.horizontalSpacing:  2
ssh-askpass.Indicator.verticalSpacing:    4
ssh-askpass.Indicator.minimumCount:       8
ssh-askpass.Indicator.maximumCount:       24

! ------------------------------------------------------------------------------
! Urxvt
! ------------------------------------------------------------------------------
! URxvt.perl-lib:				  /home/jeff/.urxvt/urxvt-perls
! URxvt*termName:				  rxvt-color
! URxvt*font:               xft:Inconsolata:pixelsize=12
! URxvt*font:				      xft:Monospace Regular:pixelsize=18:size=16:style=Book
! URxvt.letterSpace:			  -1
! URxvt.depth:              32
! URxvt.geometry:				  80x24+877+462
! URxvt.background:			  rgba:0000/0000/0000/e800
! URxvt.internalBorder:     6
! URxvt*transparent:			  True
! URxvt*shading:			  	  15
! URxvt*scrollBar:			    False
! URxvt.scrollBar_right:		False
! URxvt.scrollBar_floating:	False
! URxvt.scrollstyle:			  rxvt

! URxvt.perl-ext-common     default,resize-font,vim-insert
! URxvt.keysym.C-Up:        font-size:increase
! URxvt.keysym.C-Down:      font-size:decrease
! URxvt.keysym.C-S-Up:      font-size:incglobal
! URxvt.keysym.C-S-Down:    font-size:decglobal
! URxvt.keysym.C-equal:     font-size:reset
! URxvt.keysym.C-slash:     font-size:show
! URxvt.perl-ext-common:		  default,keyboard-select,url-select,clipboard,tabbed
! URxvt.perl-ext-common:		  default,keyboard-select,url-select,clipboard
!! keyboard-select
! URxvt.keysym.M-Escape:		  perl:keyboard-select:activate
! URxvt.keysym.M-s:			  perl:keyboard-select:search
!! url-select
! URxvt.keysym.M-u:			  perl:url-select:select_next
! URxvt.url-select.autocopy:	  true
! URxvt.url-select.launcher:	  /usr/bin/xdg-open
! URxvt.url-select.button:	  2
! URxvt.url-select.underline:	  true
!!clipboard
! URxvt.keysym.M-c:		  	  perl:clipboard:copy
! URxvt.keysym.I-c:		  	  perl:clipboard:copy
! URxvt.keysym.M-v:		  	  perl:clipboard:paste
! URxvt.keysym.I-v:		  	  perl:clipboard:paste
! URxvt.clipboard.autocopy:	  true
!! tabbed

! ------------------------------------------------------------------------------
! XTerm
! ------------------------------------------------------------------------------
XTerm*termName:				  xterm-256color

! ------------------------------------------------------------------------------
! i3 colors
! ------------------------------------------------------------------------------
*background:            #272827
*foreground:            #657b83
*fading:                15
*fadeColor:             black
*cursorColor:           #16A085
*pointerColorBackground:#2B2C2B
*pointerColorForeground:#16A085


! special
*.foreground:           #bfbfbf
*.background:           #162025
*.cursorColor:          #bfbfbf

! black
*.color0:               #0f1619
*.color8:               #365a5c

! red
*.color1:               #393843
*.color9:               #662b37

! green
*.color2:               #75404b
*.color10:              #193a48

! yellow
*.color3:               #235964
*.color11:              #393843

! blue
*.color4:               #0b4753
*.color12:              #75404b

! magenta
*.color5:               #2a474a
*.color13:              #662b37

! cyan
*.color6:               #662b37
*.color14:              #083842

! white
*.color7:               #bfbfbf
*.color15:              #bfbfbf
