# My dotfiles

My dotfiles for awesomewm, emacs, vim, zsh, tmux,...

## Here are some external programs or libraries i use
  * [dotbot](https://github.com/anishathalye/dotbot/)
  * [editorconfig](https://editorconfig.org/)
  * [Zsh IMproved FrameWork](https://github.com/zimfw/zimfw)
  * [lvim](https://www.lunarvim.org/)
